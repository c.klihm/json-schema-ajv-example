const path = require("path");
const express = require("express");
const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const ajv = require("./src/ajv");

const app = express();
const config = require("./webpack.config.js");
const compiler = webpack(config);

app.use(
    webpackDevMiddleware(compiler, {
        publicPath: config.output.publicPath,
    }),
);
app.use(express.json());
app.use("/schema", express.static(path.join(__dirname, "schema/")));

app.get("/customer", (_, res) => {
    res.sendFile(path.join(__dirname, "dist/index.html"));
});

app.post("/customer", (req, res) => {
    const schema = require("./schema/customer.json");
    const customer = req.body;

    const validate = ajv.compile(schema);
    validate(customer)
        .then(() => {
            res.status(200).end(JSON.stringify({ data: customer }));
        })
        .catch(({ errors }) => {
            res.status(400).end(JSON.stringify({ errors }));
        });
});

app.listen(3080, () => {
    console.log("Example app listening on port 3080!\n");
});

/**
 * @param {Object} data
 * @param {Object} schema
 * @return {string[]}
 */
export function getAllRequired(data, schema) {
    return getValueBasedRequired(data, schema).concat(schema.required);
}

/**
 * Ajv doesn't support the concept "required" from a form perspective, it only
 * points to missing fields in the data object, which are required by the schema.
 * But we need to know it "in time" if a field becomes required based on a runtime condition.
 * For that case this little helper function evaluates the required properties
 * based on the "const" property name of the given rule (condition).
 * @param {Object} data
 * @param {Object} schema
 * @return {string[]}
 */
export function getValueBasedRequired(data, schema) {
    const required = new Set();

    const rules = schema.allOf ? schema.allOf : schema?.if ? [schema] : [];

    for (const rule of rules) {
        let conditionMet = false;

        for (const [property, condition] of Object.entries(
            rule.if?.properties ?? {},
        )) {
            for (const name in condition) {
                conditionMet =
                    conditionMet ||
                    {
                        // as the name of the function suggests, we only support the "const" keyword
                        const: (value) => data[property] === value,
                    }[name](condition[name]);
            }
        }

        let toApply = conditionMet ? rule.then?.required ?? [] : [];

        if (rule.then?.if || rule.then?.allOf) {
            toApply = toApply.concat([
                ...new Set(getValueBasedRequired(data, rule.then)),
            ]);
        }

        toApply.forEach((property) => required.add(property));
    }

    return [...required];
}

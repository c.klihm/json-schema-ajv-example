export default class IntegerRenderer extends HTMLElement {
    constructor(props) {
        super();
        this.props = props;
        this.value = null;
        this.requiredElement = null;
        this.errorElement = null;
    }

    connectedCallback() {
        this.render();

        this.requiredElement = this.querySelector(".required");
        this.errorElement = this.querySelector(".error");
        this.errorElement.style = "display: none;";

        this.querySelector("input").addEventListener("change", (event) => {
            // An empty string would cause a type error, which could be confusing
            // so we need to reset the value to null in order
            // to generate a meaningful error message
            this.value =
                event.target.value.length > 0
                    ? parseInt(event.target.value)
                    : null;

            this.dispatchEvent(new CustomEvent("updated"));
            this.errorElement.style = "display: block;";
        });
    }

    render() {
        this.innerHTML = `
            <div class="integer-renderer">
                <label for="${this.props.id}">
                    ${this.props.id}
                    <span class="required">*</span>
                </label>
                <input type="number" id="${this.props.id}" name="${this.props.id}">
                <div class="error"></div>
            </div>
        `;
    }
}

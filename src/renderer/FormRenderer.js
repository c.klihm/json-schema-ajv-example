import { getAllRequired } from "../getRequired";

export default class FormRenderer extends HTMLElement {
    constructor(ajv, schema, renderers) {
        super();
        this.validate = ajv.compile(schema);
        this.schema = schema;
        this.renderers = renderers;
        this.form = document.createElement("form");
        this.form.classList.add("form-renderer");
        /** @type {Map<string, HTMLElement>} */
        this.controls = new Map();
    }

    connectedCallback() {
        this.render();
    }

    render() {
        this.appendChild(this.form);

        this.form.addEventListener("submit", this.handleSubmit.bind(this));

        // create one from control for each schema property
        // note that this approach does not work with conditional properties
        for (const [key, value] of Object.entries(this.schema.properties)) {
            const renderer = this.renderers.get(value.type);
            if (renderer !== undefined) {
                const instance = new renderer({ id: key });
                this.controls.set(key, instance);
                instance.addEventListener("updated", () => {
                    this.updateRequiredIndicators(this.getFormData());
                    this.updateErrorMessages();
                });
                this.form.appendChild(instance);
            }
        }
        this.updateRequiredIndicators(this.getFormData());

        const submit = document.createElement("input");
        submit.type = "submit";
        this.form.appendChild(submit);
    }

    getFormData() {
        return [...this.controls.entries()].reduce((acc, [key, control]) => {
            if (control.value !== null) {
                acc[key] = control.value;
            }

            return acc;
        }, {});
    }

    updateRequiredIndicators(data) {
        const allRequired = getAllRequired(data, this.schema);
        for (const [key, control] of this.controls.entries()) {
            control.requiredElement.style = allRequired.includes(key)
                ? "display: inline;"
                : "display: none;";
        }
    }

    async updateErrorMessages() {
        let errorMap = new Map();
        // ensure schema is $async !!!
        await this.validate(this.getFormData())
            .catch((validationResult) => {
                const formattedErrors = [];
                for (const error of validationResult.errors) {
                    if (
                        error.params?.errors &&
                        error.keyword === "errorMessage"
                    ) {
                        error.params.errors.forEach((groupedError) => {
                            groupedError.message = error.message;
                            formattedErrors.push(groupedError);
                        });
                    } else {
                        formattedErrors.push(error);
                    }
                }
                // map errors to controls
                errorMap = formattedErrors.reduce((errorMap, error) => {
                    if (error.keyword === "required") {
                        errorMap.set(
                            error.params.missingProperty,
                            error.message,
                        );
                    }
                    if (error.instancePath.length > 0) {
                        errorMap.set(
                            error.instancePath.substring(1),
                            error.message,
                        );
                    }

                    return errorMap;
                }, new Map());

            })
            .finally(() => {
                // update error messages
                for (const [key, control] of this.controls.entries()) {
                    const error = errorMap.get(key);
                    control.errorElement.textContent = error ?? "";
                }
            });
    }

    async handleSubmit(submit) {
        submit.preventDefault();

        const customer = this.getFormData();
        let errors = [];

        // ensure schema is $async !!!
        await this.validate(customer)
            .then(async () => {
                // form submission handling should be placed here, but ...
                //
                // const response = await fetch("/customer", {
                //     method: "POST",
                //     headers: {
                //         "Content-Type": "application/json",
                //     },
                //     body: JSON.stringify(customer),
                // }).then((res) => res.json());
                // errors = response.errors ?? [];
            })
            .catch((validationResult) => {
                errors = validationResult.errors;
            })
            .finally(async () => {
                // ... for demonstration purposes we will post anyway
                await fetch("/customer", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(customer),
                }).then((res) => res.json());
            })
            .catch((validationResult) => {
                errors = validationResult.errors;
            })
            .finally(() => {
                // reformat errors
                const formattedErrors = [];
                for (const error of errors) {
                    if (
                        error.params?.errors &&
                        error.keyword === "errorMessage"
                    ) {
                        error.params.errors.forEach((groupedError) => {
                            groupedError.message = error.message;
                            formattedErrors.push(groupedError);
                        });
                    } else {
                        formattedErrors.push(error);
                    }
                }

                // map errors to controls
                const errorMap = formattedErrors.reduce((errorMap, error) => {
                    if (error.keyword === "required") {
                        errorMap.set(
                            error.params.missingProperty,
                            error.message,
                        );
                    }
                    if (error.instancePath.length > 0) {
                        errorMap.set(
                            error.instancePath.substring(1),
                            error.message,
                        );
                    }

                    return errorMap;
                }, new Map());

                // update error messages
                for (const [key, control] of this.controls.entries()) {
                    const error = errorMap.get(key);
                    control.errorElement.textContent = error ?? "";
                    control.errorElement.style = "display: block;";
                }

                console.log("--- Errors ---");
                console.table(Object.fromEntries(errorMap.entries()));

                console.log("--- Data ---");
                console.table(customer);
            });

        this.updateRequiredIndicators(customer);
    }
}

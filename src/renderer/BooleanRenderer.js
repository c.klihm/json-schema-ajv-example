export default class BooleanRenderer extends HTMLElement {
    constructor(props) {
        super();
        this.props = props;
        this.value = false;
        this.requiredElement = null;
        this.errorElement = null;
    }

    connectedCallback() {
        this.render();

        this.requiredElement = this.querySelector(".required");
        this.errorElement = this.querySelector(".error");
        this.errorElement.style = "display: none;";

        const input = this.querySelector("input");
        input.checked = this.value;

        input.addEventListener("change", (event) => {
            this.value = event.target.checked;
            this.dispatchEvent(new CustomEvent("updated"));
            this.errorElement.style = "display: block;";
        });
    }

    render() {
        this.innerHTML = `
            <div class="boolean-renderer">
                <label for="${this.props.id}">
                    ${this.props.id}
                    <span class="required">*</span>
                </label>
                <input type="checkbox" id="${this.props.id}" name="${this.props.id}">
                <div class="error"></div>
            </div>
        `;
    }
}

import ajv from "./ajv";
import FormRenderer from "./renderer/FormRenderer";
import BooleanRenderer from "./renderer/BooleanRenderer";
import IntegerRenderer from "./renderer/IntegerRenderer";
import StringRenderer from "./renderer/StringRenderer";

customElements.define("form-renderer", FormRenderer);
customElements.define("boolean-renderer", BooleanRenderer);
customElements.define("integer-renderer", IntegerRenderer);
customElements.define("string-renderer", StringRenderer);

(async function main() {
    const schema = await fetch("/schema/customer.json").then((res) =>
        res.json(),
    );
    document.body.appendChild(
        new FormRenderer(
            ajv,
            schema,
            new Map([
                ["boolean", BooleanRenderer],
                ["integer", IntegerRenderer],
                ["string", StringRenderer],
            ]),
        ),
    );
})();

const ajv = new (require("ajv"))({
    allErrors: true,
});
require("ajv-errors")(ajv);
require("ajv-formats")(ajv, ["email"]);

module.exports = ajv;

# JSON-Schema - AJV Example

This is an example of using [AJV](https://ajv.js.org/) to validate dynamic HTML-Forms based on a JSON-Schema.

## Setup

```bash
yarn install
```

## Development

```bash
yarn dev
```

See [package.json](package.json) for details.
